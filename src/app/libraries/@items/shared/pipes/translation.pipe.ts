import {Pipe, PipeTransform} from '@angular/core';

@Pipe({name: 'trans'})
export class TranslationPipe implements PipeTransform {
    transform(value:string, exponent:string) {
        return value + '....' + exponent;
    }
}