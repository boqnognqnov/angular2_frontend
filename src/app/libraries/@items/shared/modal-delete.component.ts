import {Component, Input} from '@angular/core';


declare var dialogPolyfill:any;

@Component({
    selector: 'shared-modal',
    templateUrl: './templates/modal-delete.component.html',
    styleUrls: ['./templates/modal-delete.component.css']
})
export class ModalDeleteComponent {

    @Input() content = [];
    @Input() parameter_1 = null;
    @Input() parameter_2 = null;

    constructor() {

    }
    
    deleteAction(id) {
        var component = this.content['component'];
        var componentName = component.constructor.name;
        // console.log(componentName);
        // console.log(id);

        switch (componentName) {
            case "HomePageComponent":
                //parameter_1 IS KIND ID
                component.deleteKind(this.parameter_1);
                break;

            case "AnimalListComponent":
                //parameter_1 IS ANIMAL ID
                //parameter_2 IS KIND NAME
                component.deleteAnimal(this.parameter_1, this.parameter_2);
                break;

        }

    }

}