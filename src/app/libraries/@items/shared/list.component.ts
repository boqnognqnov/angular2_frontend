import {Component, Input} from '@angular/core';

@Component({
    selector: 'shared-list',
    templateUrl: './templates/list-view.component.html',
})
export class ListComponent {

    @Input() items = [];

    constructor() {

    }


}