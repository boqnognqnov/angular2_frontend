import 'rxjs/add/operator/map';
import {Http, Headers, RequestOptions, Response, URLSearchParams} from '@angular/http';
// import {LocalStorage} from '@docebo/lib/services/local-storage.service';
import {Injectable} from '@angular/core';
import {Observable} from "rxjs/Rx";
import {Router} from '@angular/router'
// import {Session} from '@docebo/lib/models/session.model';
import {environment} from '../../../../environments/environment-dev';

@Injectable()
/**
 * Injectable service to communicate with the backend Hydra API
 */
export class BackendService {

    private headers: Headers;

    private apiUrl:string = environment.apiUrl;

    /**
     * This services relies on two lower level services, therefore we need to inject them as dependencies
     * @param _http
     * @param _localStorage
     */
    constructor(private http:Http, public router:Router) {
        this.headers = new Headers();
        this.headers.append('Access-Control-Allow-Headers', 'Content-Type, X-XSRF-TOKEN');
    }

    /**
     * Performs a backend API call
     *
     * @param {String} url requested url
     * @param {String} method The HTTP method to use (POST, GET ... etc)
     * @param {Object} payload An optional input payload for the API request
     */
    request(url:string, method:string, payload?:{}, searchParams?:{}, content_type:string = 'application/json; charset=UTF-8') {
        let params = new URLSearchParams();
        let getParams = (method === 'GET' && payload) ? payload : searchParams;

        for (var name in getParams)
            params.set(name, getParams[name]);

        // Prepare HTTP headers
        var headers = new Headers();
        if (content_type !== "undefined") {
            headers.append('Content-Type', content_type);
        }


        // Check if we have a subfolder in the URL (i.e. multidomain client) and append it to the API request
        // so that backend is able to recognize the multidomain client

        // Check if we have a developement API url in process.env
        if (this.apiUrl && !(/^https?:\/\/|^\/\//i.test(url)))
            url = this.apiUrl + url;

        // Perform the actual call and return a json response object
        return this.http.request(url, new RequestOptions({
            method: method,
            headers: headers,
            body: payload,
            search: params
        })).map(response => response.json()).catch((err) => {
            return this.handleApiErrors(err);
        });
    }


    public redirectTo(path:string, withParameter = false, parameter = '') {
        console.log('navigate to -> ' + path);
        if (withParameter == false) {
            this.router.navigate(['/' + path]);
        } else {
            this.router.navigate(['/' + path, parameter]);
        }
    }

    /**
     * Handler function called for each backend API
     * @param err
     * @returns {ErrorObservable}
     */
    private handleApiErrors(err) {
        switch (err.status) {
            case 401:

                break;

            case 500:

                break;

            case 403:
                // this.router.navigate(['unauthorized']);
                break;

            case 400:
                break;
        }

        return Observable.throw(err.json());
    }
}