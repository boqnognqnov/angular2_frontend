import {Component} from '@angular/core';
import {AnimalKindService} from "./modules/services/animal-kind.service";
import {AnimalKind} from "./modules/models/animal-kind";


@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
})
export class AppComponent {

    kinds:Array<AnimalKind>;
    title = 'Main app content';

    constructor(private _animalKindService:AnimalKindService) {

    }


    ngOnInit() {

        this._animalKindService.getAllKinds().subscribe((response) => {
                this.kinds = AnimalKindService.bindParamsToKindObj(response);
                // console.log(JSON.stringify(this.kinds));
            }
        );


    }
}


