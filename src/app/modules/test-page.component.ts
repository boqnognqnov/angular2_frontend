import {Component} from '@angular/core';
import {TranslateService} from 'ng2-translate';
import {TranslateTextService} from "./services/translate-text.service";


@Component({
    templateUrl: './pages/test-page.component.html',


})
export class TestPageComponent {

    param:string = 'world';

    private langCodes = [];

    private currentLangCode:string;

    private translateObj:TranslateService;

    constructor(translate:TranslateService) {
        // this language will be used as a fallback when a translation isn't found in the current language

        this.translateObj = translate;

        if (this.currentLangCode == null) {
            this.currentLangCode = TranslateTextService.defaultLangCode;
        }

        this.changeTextByLang(this.currentLangCode);
    }

    changeTextByLang(langCode) {
        // langCode = 'bg';
        this.translateObj.setDefaultLang(langCode);


        this.translateObj.setTranslation(langCode, TranslateTextService.getTranslations(langCode));

        // the lang to use, if the lang isn't available, it will use the current loader to get them
        this.translateObj.use(langCode);
    }


    ngOnInit() {
        this.langCodes = TranslateTextService.getAvailableLangCodes();
        console.log(this.langCodes);

    }

}