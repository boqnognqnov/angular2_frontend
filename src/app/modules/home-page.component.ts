import {Component} from '@angular/core';
import {BackendService} from '@items/lib/backend.service';
import {AnimalKind} from "./models/animal-kind";
import {AnimalKindService} from "./services/animal-kind.service";


declare var dialogPolyfill:any;

@Component({
    templateUrl: './pages/home.component.html'

})
export class HomePageComponent {

    home_list:JSON = null;
    del_modal_content = [];


    kinds:Array<AnimalKind>;

    selectedKind:AnimalKind;

    globalErrors = [];

    constructor(private _backendService:BackendService,
                private _animalKindService:AnimalKindService) {

    }


    addEditKind(selectedKindObj) {
        if (AnimalKindService.validateKindForm(selectedKindObj)['status'] == false) {
            this.globalErrors = AnimalKindService.validateKindForm(selectedKindObj);
            return;
        }
        this._backendService.request('kind_edit_create', 'POST', selectedKindObj, {}).map(itemsResponse => itemsResponse).subscribe((response) => {
            this._animalKindService.getAllKinds().subscribe((response) => {
                    this.kinds = AnimalKindService.bindParamsToKindObj(response);
                }
            );
            this.resetForm();
        });

    }


    resetForm() {

        this.selectedKind.id = 0;
        this.selectedKind.kind_name = '';
        this.globalErrors = [];
    }


    deleteModal(kind_id) {
        var currentComponent = this;

        var dialog:any = document.querySelector('dialog');
        if (!dialog.showModal) {
            dialogPolyfill.registerDialog(dialog);
        }

        dialog.showModal();

        dialog.querySelector('.close').addEventListener('click', function () {
            if (dialog.hasAttribute('open')) {
                dialog.close();
            }

        });

        dialog.querySelector('.agree').addEventListener('click', function () {
            currentComponent.deleteKind(kind_id);
            if (dialog.hasAttribute('open')) {
                dialog.close();
            }
        });


    }

    deleteKind(kind_id) {
        console.log(kind_id);
        this._backendService.request('kind_delete/' + kind_id, 'POST').map(itemsResponse => itemsResponse).subscribe((response) => {
            this._animalKindService.getAllKinds().subscribe((response) => {
                    this.kinds = AnimalKindService.bindParamsToKindObj(response);
                    this.resetForm();
                }
            );
        });
    }


    bindSelectedKind(selectedKindObj) {
        // after
        // {id: "2", kind: "cat"}
        this.selectedKind = new AnimalKind(selectedKindObj.id, selectedKindObj.kind_name);
    }


    ngOnInit() {
        this.selectedKind = new AnimalKind();


        this.del_modal_content['question'] = "Are you sure to want to delete this category ?";
        this.del_modal_content['component'] = this;


        this._backendService.request('home_page', 'GET').map(itemsResponse => itemsResponse).subscribe((response) => {

           // console.log(response);
            this.home_list = response;
        });

        this._animalKindService.getAllKinds().subscribe((response) => {
                this.kinds = AnimalKindService.bindParamsToKindObj(response);
            }
        );


    }

}
