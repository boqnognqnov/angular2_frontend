export class TranslateTextService {

    static defaultLangCode:string = 'en';

    static translations = {
        en: {
            HELLO: 'hello {{value}}',
            TEST: 'AAAA {{value}}',
        },
        bg: {
            HELLO: 'Здравей {{value}}',
            TEST: 'ЯЯЯЯ {{value}}',
        },

    };

    static  getTranslations(langCode):JSON {
        return this.translations[langCode];
    }

    static  getAvailableLangCodes() {
        var codes = [];
        for (var code in this.translations) {
            codes.push(code);
        }
        return codes;
    }


}