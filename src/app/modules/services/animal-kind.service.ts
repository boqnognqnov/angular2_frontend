import {Animal} from "../models/animal";
import {BackendService} from "../../libraries/@items/lib/backend.service";
// import {Inject} from '@angular/core';
import {Injectable} from '@angular/core';
import {AnimalKind} from "../models/animal-kind";

@Injectable()
export class AnimalKindService {


    constructor(private _backendService:BackendService) {

    }
    
    getAllKinds() {
        return this._backendService.request('getKinds', 'GET').map(itemsResponse => itemsResponse);
    }

    static bindParamsToKindObj(response) {
        var result:Array<AnimalKind> = [];
        response.forEach((kind)=> {
            result.push(new AnimalKind(kind.id, kind.kind_name));
        });
        return result;
    }


    static validateKindForm(kind) {

        var result = [];
        result['status'] = true;
        result['errors'] = [];

        if (kind.kind_name.replace(/\s/g, "") == "") {
            result['status'] = false;
            result['errors'].push('The kind can\'t be empty !');
        }


        return result;
    }
}