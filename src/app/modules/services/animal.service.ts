import {Animal} from "../models/animal";
import {BackendService} from "../../libraries/@items/lib/backend.service";
// import {Inject} from '@angular/core';
import {Injectable} from '@angular/core';

@Injectable()
export class AnimalService {


    constructor(private _backendService:BackendService) {

    }

    static animal:Animal = new Animal();


    getAnimalById(id) {

        return this._backendService.request('animal_view/' + id, 'GET').map(itemsResponse => itemsResponse);
    }

    static bindResponseToAnimalObj(response):Animal {

        this.animal.id = response.id;
        this.animal.name = response.name;
        this.animal.description = response.description;
        this.animal.image = response.image;
        this.animal.kind_id = response.kind_id;

        return this.animal;


    }

    addEditAnimal(animalObj) {
        var requestData = {"add_edit_animal": animalObj};
        return this._backendService.request('add_edit/animal', 'POST', requestData, {}).map(itemsResponse => itemsResponse);
    }


    static validateAnimalForm(animal) {

        var data = [];
        data['status'] = true;
        data['errors'] = [];

        if (animal['kind_id'] == '') {
            data['errors'].push('Please select kind');
            data['status'] = false;
        }

        if (animal['name'] == '') {
            data['errors'].push('Please type the name');
            data['status'] = false;
        }

        if (animal['description'] == '') {
            data['errors'].push('Please type the description');
            data['status'] = false;
        }

        // if (!animal['image'].match(/[https?:\/\/(?:www\.|(?!www))[^\s\.]+\.[^\s]{2,}|www\.[^\s]+\.[^\s]{2,}]/)) {
        if (!animal['image'].match(/(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/)) {
            data['errors'].push('Please insert image url');
            data['status'] = false;
        }


        return data;
    }
}