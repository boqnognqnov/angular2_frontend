import {Component, Input} from '@angular/core';
import {BackendService} from '@items/lib/backend.service';
import {Animal} from './models/animal';
import {ActivatedRoute} from "@angular/router";
import {AnimalService} from "./services/animal.service";
import {AnimalKindService} from "./services/animal-kind.service";
import {AnimalKind} from "./models/animal-kind";
// import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

@Component({
    templateUrl: './pages/add-edit-animal.component.html',
})
export class AddEditAnimalComponent {

    kinds:Array<AnimalKind>;
    animal:Animal = new Animal();

    globalErrors = [];


    constructor(private _animalKindService:AnimalKindService,
                private _backendService:BackendService,
                private route:ActivatedRoute,
                private _animalService:AnimalService) {

    }


    addEditAnimal(animalObj:Animal) {

        var validation = AnimalService.validateAnimalForm(animalObj);
        if (validation['status'] == false) {
            this.globalErrors = validation;
            return;
        }

        this._animalService.addEditAnimal(animalObj).subscribe((response) => {
            if (response.status == "success") {
                this._backendService.redirectTo('animal/list/' + response.data.kind_id);
            }
        });


    }

    ngOnInit() {
        var id:number = 0;


        this.route.params.subscribe(params => {
            id = +params['id']; // (+) converts string 'id' to a number
        });


        if (id != 0) {
            this._animalService.getAnimalById(id).subscribe((response) => {
                this.animal = AnimalService.bindResponseToAnimalObj(response);
            });
        }

        this._animalKindService.getAllKinds().subscribe((response) => {
                this.kinds = AnimalKindService.bindParamsToKindObj(response);
            }
        );


    }
}
