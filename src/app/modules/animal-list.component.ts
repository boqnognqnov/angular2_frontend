import {Component} from '@angular/core';
import {BackendService} from '@items/lib/backend.service';
import {ActivatedRoute} from '@angular/router';


@Component({
    templateUrl: './pages/animal-list.component.html',
    styleUrls: ['./pages/styles/animal-list.component.css']

})
export class AnimalListComponent {

    animals:JSON = null;
    del_modal_content = [];


    constructor(private _backendService:BackendService, private route:ActivatedRoute) {

    }

    deleteAnimal(animalId:number, kind:string) {

        this._backendService.request('animal_delete/' + animalId, 'POST').map(itemsResponse => itemsResponse).subscribe((response) => {

            this._backendService.request('animal/' + kind, 'GET').map(itemsResponse => itemsResponse).subscribe((response) => {
                this.animals = response;
            });

        });
    }


    ngOnInit() {

        var kind:string;

        this.del_modal_content['question'] = "Are you sure to want to delete this animal ?";
        this.del_modal_content['component'] = this;

        this.route.params.subscribe(params => {
            kind = params['kind']; // (+) converts string 'id' to a number

            this._backendService.request('animal/' + kind, 'GET').map(itemsResponse => itemsResponse).subscribe((response) => {
                this.animals = response;
            });
        });

    }

}