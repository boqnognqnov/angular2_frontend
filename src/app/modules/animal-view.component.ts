import {Component, OnInit, OnDestroy} from '@angular/core';
import {BackendService} from '@items/lib/backend.service';
import {ActivatedRoute} from '@angular/router';
import {Animal} from "./models/animal";
import {AnimalService} from "./services/animal.service";


@Component({
    templateUrl: './pages/animal-view.component.html',


})
export class AnimalViewComponent {


    animal:Animal = new Animal();


    constructor(private _backendService:BackendService, private route:ActivatedRoute, private _animalService:AnimalService) {

    }

    ngOnInit() {

        var id:number;


        this.route.params.subscribe(params => {
            id = +params['id']; // (+) converts string 'id' to a number
        });


        this._animalService.getAnimalById(id).subscribe((response) => {
            this.animal = AnimalService.bindResponseToAnimalObj(response);
        });


    }

}