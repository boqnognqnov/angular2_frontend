import {Component} from '@angular/core';
import {FavoriteService} from './services/favorite.service'

@Component({
    selector: 'favorite',
    templateUrl: './pages/favorite.component.html',
    providers: [FavoriteService]

})
export class FavoriteComponent {

    isFavorite = false;
    
    onClick() {
        this.isFavorite = !this.isFavorite;
    }

}