export class Animal {

    constructor(public id:number = 0,
                public kind_name:string = '',
                public kind_id:string = '',
                public name:string = '',
                public description:string = '',
                public image:string = '') {
    }
    

}
