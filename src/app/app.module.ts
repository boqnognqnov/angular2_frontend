import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule, Http} from '@angular/http';

import {AppComponent} from './app.component';
import {FavoriteComponent} from "./modules/favorite.component";
import {AnimalListComponent} from "./modules/animal-list.component";
import {AnimalViewComponent} from "./modules/animal-view.component";
import {AddEditAnimalComponent} from "./modules/add-edit-animal.component";
import {HomePageComponent} from "./modules/home-page.component";
import {RouterModule}   from '@angular/router';
import {BackendService} from "./libraries/@items/lib/backend.service";
import {ListComponent} from "./libraries/@items/shared/list.component";
import {AnimalService} from "./modules/services/animal.service";
import {AnimalKindService} from "./modules/services/animal-kind.service";
import {TestPageComponent} from "./modules/test-page.component";
import {ModalDeleteComponent} from "./libraries/@items/shared/modal-delete.component";
import {TranslateModule, TranslateStaticLoader, TranslateLoader} from 'ng2-translate';
import {TranslationPipe} from "./libraries/@items/shared/pipes/translation.pipe";


export const routeConfig = [
    {
        path: '',
        children: [
            {path: '', component: HomePageComponent},
            {path: 'animal/list/:kind', component: AnimalListComponent},
            {path: 'animal/view/:id', component: AnimalViewComponent},
            {path: 'animal/create_update/:id', component: AddEditAnimalComponent},
            {path: 'test-page', component: TestPageComponent}
        ]
    }
];

@NgModule({
    declarations: [
        AppComponent,
        FavoriteComponent,
        HomePageComponent,
        AnimalListComponent,
        AnimalViewComponent,
        ListComponent,
        ModalDeleteComponent,
        AddEditAnimalComponent,
        TestPageComponent,
        TranslationPipe

    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        RouterModule.forRoot(routeConfig),
        TranslateModule.forRoot({
            provide: TranslateLoader,
            useFactory: (http:Http) => new TranslateStaticLoader(http, '/assets/i18n', '.json'),
            deps: [Http]
        })
        // TranslateModule.forRoot()

    ],
    providers: [
        BackendService,
        AnimalService,
        AnimalKindService
    ],

    bootstrap: [AppComponent]
})
export class AppModule {
}
